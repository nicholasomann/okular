msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-03 00:44+0000\n"
"PO-Revision-Date: 2022-11-06 01:06\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/okular/org.kde.active.documentviewer."
"pot\n"
"X-Crowdin-File-ID: 5819\n"

#: package/contents/ui/Bookmarks.qml:20 package/contents/ui/OkularDrawer.qml:85
msgid "Bookmarks"
msgstr "书签"

#: package/contents/ui/CertificateViewerDialog.qml:21
msgid "Certificate Viewer"
msgstr "证书查看器"

#: package/contents/ui/CertificateViewerDialog.qml:32
msgid "Issued By"
msgstr "颁发者："

#: package/contents/ui/CertificateViewerDialog.qml:37
#: package/contents/ui/CertificateViewerDialog.qml:65
msgid "Common Name:"
msgstr "通用名称："

#: package/contents/ui/CertificateViewerDialog.qml:43
#: package/contents/ui/CertificateViewerDialog.qml:71
msgid "EMail:"
msgstr "电子邮件："

#: package/contents/ui/CertificateViewerDialog.qml:49
#: package/contents/ui/CertificateViewerDialog.qml:77
msgid "Organization:"
msgstr "组织："

#: package/contents/ui/CertificateViewerDialog.qml:60
msgid "Issued To"
msgstr "颁发给："

#: package/contents/ui/CertificateViewerDialog.qml:88
msgid "Validity"
msgstr "有效性"

#: package/contents/ui/CertificateViewerDialog.qml:93
msgid "Issued On:"
msgstr "颁发时间："

#: package/contents/ui/CertificateViewerDialog.qml:99
msgid "Expires On:"
msgstr "失效时间："

#: package/contents/ui/CertificateViewerDialog.qml:110
msgid "Fingerprints"
msgstr "指纹"

#: package/contents/ui/CertificateViewerDialog.qml:115
msgid "SHA-1 Fingerprint:"
msgstr "SHA-1 指纹："

#: package/contents/ui/CertificateViewerDialog.qml:121
msgid "SHA-256 Fingerprint:"
msgstr "SHA-256 指纹："

#: package/contents/ui/CertificateViewerDialog.qml:135
msgid "Export..."
msgstr "导出..."

#: package/contents/ui/CertificateViewerDialog.qml:141
#: package/contents/ui/SignaturePropertiesDialog.qml:148
msgid "Close"
msgstr "关闭"

#: package/contents/ui/CertificateViewerDialog.qml:149
msgid "Certificate File (*.cer)"
msgstr "证书文件 (*.cer)"

#: package/contents/ui/CertificateViewerDialog.qml:164
#: package/contents/ui/SignaturePropertiesDialog.qml:168
msgid "Error"
msgstr "错误"

#: package/contents/ui/CertificateViewerDialog.qml:166
msgid "Could not export the certificate."
msgstr "无法导出证书。"

#: package/contents/ui/main.qml:23 package/contents/ui/main.qml:64
msgid "Okular"
msgstr "Okular"

#: package/contents/ui/main.qml:40
msgid "Open..."
msgstr "打开..."

#: package/contents/ui/main.qml:47
msgid "About"
msgstr "关于"

#: package/contents/ui/main.qml:104
msgid "Password Needed"
msgstr "需要输入密码"

#: package/contents/ui/MainView.qml:25
msgid "Remove bookmark"
msgstr "移除书签"

#: package/contents/ui/MainView.qml:25
msgid "Bookmark this page"
msgstr "添加此页到书签"

#: package/contents/ui/MainView.qml:82
msgid "No document open"
msgstr "没有打开的文档"

#: package/contents/ui/OkularDrawer.qml:57
msgid "Thumbnails"
msgstr "缩略图"

#: package/contents/ui/OkularDrawer.qml:71
msgid "Table of contents"
msgstr "目录"

#: package/contents/ui/OkularDrawer.qml:99
msgid "Signatures"
msgstr "签名"

#: package/contents/ui/SignaturePropertiesDialog.qml:30
msgid "Signature Properties"
msgstr "签名属性"

#: package/contents/ui/SignaturePropertiesDialog.qml:44
msgid "Validity Status"
msgstr "有效性状态"

#: package/contents/ui/SignaturePropertiesDialog.qml:50
msgid "Signature Validity:"
msgstr "签名有效性："

#: package/contents/ui/SignaturePropertiesDialog.qml:56
msgid "Document Modifications:"
msgstr "文档修改："

#: package/contents/ui/SignaturePropertiesDialog.qml:63
msgid "Additional Information"
msgstr "额外信息"

#: package/contents/ui/SignaturePropertiesDialog.qml:72
msgid "Signed By:"
msgstr "签名者："

#: package/contents/ui/SignaturePropertiesDialog.qml:78
msgid "Signing Time:"
msgstr "签名时间："

#: package/contents/ui/SignaturePropertiesDialog.qml:84
msgid "Reason:"
msgstr "原因："

#: package/contents/ui/SignaturePropertiesDialog.qml:91
msgid "Location:"
msgstr "位置："

#: package/contents/ui/SignaturePropertiesDialog.qml:100
msgid "Document Version"
msgstr "文档版本"

#: package/contents/ui/SignaturePropertiesDialog.qml:110
msgctxt "Document Revision <current> of <total>"
msgid "Document Revision %1 of %2"
msgstr "文档修订版 %1，共 %2 版"

#: package/contents/ui/SignaturePropertiesDialog.qml:114
msgid "Save Signed Version..."
msgstr "保存已签名版本..."

#: package/contents/ui/SignaturePropertiesDialog.qml:128
msgid "View Certificate..."
msgstr "查看证书..."

#: package/contents/ui/SignaturePropertiesDialog.qml:170
msgid "Could not save the signature."
msgstr "无法保存签名。"

#: package/contents/ui/Signatures.qml:27
msgid "Not Available"
msgstr "不可用"

#: package/contents/ui/ThumbnailsBase.qml:43
msgid "No results found."
msgstr "无搜索结果。"
